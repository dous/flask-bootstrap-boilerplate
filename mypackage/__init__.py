from flask import Flask, make_response
from ext import templated

app = Flask(__name__)


@app.route("/")
@templated()
def index():
    pass
